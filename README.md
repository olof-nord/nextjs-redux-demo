# nextjs-redux-demo

A demo web application using Next.js and Redux together.

The with-redux-store library includes redux on all pages,
once per page though the Layout component.

More to this setup can be found here:
https://jibin.tech/preserve-prerendering-in-nextjs-when-using-redux/

The demo uses typescript and simplifies and streamlines the redux setup with the Redux Toolkit.
