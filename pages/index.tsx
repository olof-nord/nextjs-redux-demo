import React from "react";
import { NextPage } from "next";

import Counter from "../src/components/Counter";

function indexPage(): NextPage {
  return (
    <div>
      <Counter/>
    </div>
  );
};

export default indexPage;