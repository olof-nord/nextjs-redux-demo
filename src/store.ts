import { configureStore } from "@reduxjs/toolkit"
import { rootReducer } from "../src/reducers";

export default function initializeStore(initialState = {}) {
  return configureStore({
    reducer: rootReducer,
    preloadedState: initialState
  });
}