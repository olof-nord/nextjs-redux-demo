import React, { ReactNode } from "react";
import { withRedux } from "../../lib/with-redux-store";

type Props = {
  children?: ReactNode
}

const Layout = ({ children }: Props) => (
  <div>{children}</div>
)

export default withRedux(Layout);