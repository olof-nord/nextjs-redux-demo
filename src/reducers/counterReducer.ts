import { createReducer } from "@reduxjs/toolkit"
import { increment, decrement, reset } from "../actions/counterActions";

export interface CounterState {
  counter: number
}

export const initialState: CounterState = {
  counter: 0
};

export const counterReducer = createReducer(initialState, (builder) => {

  builder.addCase(increment, (state: CounterState, action) => {
    state.counter++
  });

  builder.addCase(decrement, (state: CounterState, action) => {
    state.counter--
  });

  builder.addCase(reset, (state: CounterState   , action) => {
    state.counter = 0
  });
});